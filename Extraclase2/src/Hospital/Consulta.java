package Hospital;

import java.time.LocalDate;

public class Consulta {
	private LocalDate FechaConsulta = null;
	private String Sintomas = "";
	private double Temperatura = 0f;
	private float Peso = 0f;
	private double Estatura = 0f;
	
	public Consulta () {
		super();
	}
	public Consulta (LocalDate FechaConsulta, String Sintomas,double Temperatura,float Peso, double Estatura) {
		super();
		this.FechaConsulta = FechaConsulta;
		this.Sintomas = Sintomas;
		this.Temperatura = Temperatura;
		this.Peso = Peso;
		this.Estatura = Estatura;
		}
	public LocalDate getFechaConsulta () {
		return this.FechaConsulta;
	}
	public void setFechaConsulta (LocalDate newFechaConsulta) {
		this.FechaConsulta = newFechaConsulta;
	}
	public String getSintomas () {
		return this.Sintomas;
	}
	public void setSintomas (String newSintomas) {
		this.Sintomas = newSintomas;
	}
	public double getTemperatura () {
		return this.Temperatura;
	}
	public void setTemperatura (double newTemperatura) {
		this.Temperatura = newTemperatura;
	}
	public float getPeso () {
		return this.Peso;
	}
	public void setPeso (float newPeso) {
		this.Peso = newPeso;
	}
	public double getEstatura () {
		return this.Estatura;
	}
	public void setEstatura (double newEstatura) {
		this.Estatura = newEstatura;
	}
	public String toString() {
		return "Consulta [FechaConsulta=" + FechaConsulta + ", Sintomas=" + Sintomas + ", Temperatura=" + Temperatura
				+ ", Peso=" + Peso + ", Estatura=" + Estatura + "]";
	}
	

}

package Hospital;

import java.time.LocalDate;
import Hospital.Expedientes;
import Hospital.Consulta;
import Hospital.DiagnosticoMedico;
public class Myapp {
	
	public static void run () {
		Expedientes E = new Expedientes ("Fernando","ENSF0001277HO",LocalDate.of(2000,01,27));
		
		Consulta C = new Consulta (LocalDate.of(2021, 10, 03),"Dolor de Cabeza",38.5,81,1.78);
		
		DiagnosticoMedico DM = new DiagnosticoMedico ("Dolor de cabeza","Paracetamol","Tomar antes y despues de dormir");
		System.out.print(E);
		System.out.print(C);
		System.out.print(DM);
	}
	public static void main(String[] args) {
		run();
	}

}

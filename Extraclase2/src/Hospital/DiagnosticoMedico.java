package Hospital;

public class DiagnosticoMedico {
	private String Padecimientos = "";
	private String Medicamentos = "";
	private String Recomendaciones = "";

public DiagnosticoMedico () {
	super();
}
public DiagnosticoMedico (String Padecimientos, String Medicamentos,String Recomendaciones) {
	super();
	this.Padecimientos = Padecimientos;
	this.Medicamentos = Medicamentos;
	this.Recomendaciones = Recomendaciones;
	}
public String getPadecimientos () {
	return this.Padecimientos;
}
public void setPadecimientos (String newPadecimientos) {
	this.Padecimientos = newPadecimientos;
}
public String getMedicamentos () {
	return this.Medicamentos;
}
public void setMedicamentos (String newMedicamentos) {
	this.Medicamentos = newMedicamentos;
}
public String getRecomendaciones () {
	return this.Recomendaciones;
}
public void setRecomendaciones (String newRecomendaciones) {
	this.Recomendaciones = newRecomendaciones;
}
public String toString() {
	return "DiagnosticoMedico [Padecimientos=" + Padecimientos + ", Medicamentos=" + Medicamentos + ", Recomendaciones="
			+ Recomendaciones + "]";
}

}
